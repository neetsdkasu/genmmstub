package genmmstub;

class PythonParser extends Parser
{
    @Override
    public void parse(String line)
    {
        line = line.trim();
        int pos = line.indexOf(':');
        if (pos < 0) return;

        String kind = line.substring(0, pos);
        String text = line.substring(pos + 1).trim();

        switch (kind)
        {
        case "Class":
            setClassName(text);
            break;
        case "Method":
            setNewMethod(text);
            break;
        case "Returns":
            setReturnType(text);
            break;
        case "Parameters":
            for (String token : text.split(","))
            {
                addParamType(token);
            }
            break;
        case "Method signature":
            text = text.substring(text.indexOf('(') + 1);
            text = text.substring(0, text.lastIndexOf(')'));
            String[] params = text.split(",");
            for (int i = 1; i < params.length; i++)
            {
                addParamName(params[i].trim());
            }
            break;
        }
    }

    @Override
    public Type parseType(String token)
    {
        token = token.trim();
        if ("".equals(token)) return Type.VOID;
        switch (token)
        {
        case "integer":
            return Type.INT;
        case "tuple (integer)":
            return Type.INT_ARRAY;
        case "long":
            return Type.LONG;
        case "tuple (long)":
            return Type.LONG_ARRAY;
        case "float":
            return Type.DOUBLE;
        case "tuple (float)":
            return Type.DOUBLE_ARRAY;
        case "string":
            return Type.STRING;
        case "tuple (string)":
            return Type.STRING_ARRAY;
        case "string (char)":
            return Type.CHAR;
        case "bool": /* ? */
            return Type.BOOL;
        default:
            throw new IllegalArgumentException("token: " + token);
        }
    }
}

