package genmmstub;

interface ClassInfo
{
    String getClassName();
    Method[] getMethods();
}