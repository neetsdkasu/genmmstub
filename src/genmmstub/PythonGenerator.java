package genmmstub;

import java.io.PrintStream;

class PythonGenerator extends Generator
{
    static final String TAB = "    ";

    @Override
    public String getExtension()
    {
        return ".py";
    }

    @Override
    public void generateCodeTo(PrintStream out)
    {

        out.printf("class %s:%n", info.getClassName());

        for (Method method : info.getMethods())
        {
            out.println(TAB);
            out.printf(TAB + "def %s(self", method.getName());
            int c = method.paramCount();
            for (int i = 0; i < c; i++)
            {
                out.printf(", %s", method.getParamName(i));
            }
            out.println("):");
            if (method.getReturnType() != Type.VOID)
            {
                out.printf(TAB + TAB + "return %s%n",
                    getDefaultValue(method.getReturnType()));
            }
        }

        out.println();
        out.println("# " + CUT_LINE);

        out.println();
        out.println("from sys import stdout");

        // Method Callers
        for (Method method : info.getMethods())
        {
            out.println();
            out.printf("def %s(%s):%n",
                genCallerName(method.getName()),
                genInstanceName());
            out.println(TAB);

            int c = method.paramCount();
            for (int i = 0; i < c; i++)
            {
                out.print(TAB);
                switch (method.getParamType(i))
                {
                case INT:
                    out.printf("%s = int(raw_input())%n",
                        method.getParamName(i));
                    break;
                case INT_ARRAY:
                    out.printf("%sSize_ = int(raw_input())%n",
                        method.getParamName(i));
                    out.printf(TAB + "%1$s = [int(raw_input()) for i in xrange(%1$sSize_)]%n",
                        method.getParamName(i));
                    break;
                case LONG:
                    out.printf("%s = long(raw_input())%n",
                        method.getParamName(i));
                    break;
                case LONG_ARRAY:
                    out.printf("%sSize_ = int(raw_input())%n",
                        method.getParamName(i));
                    out.printf(TAB + "%1$s = [long(raw_input()) for i in xrange(%1$sSize_)]%n",
                        method.getParamName(i));
                    break;
                case DOUBLE:
                    out.printf("%s = float(raw_input())%n",
                        method.getParamName(i));
                    break;
                case DOUBLE_ARRAY:
                    out.printf("%sSize_ = int(raw_input())%n",
                        method.getParamName(i));
                    out.printf(TAB + "%1$s = [float(raw_input()) for i in xrange(%1$sSize_)]%n",
                        method.getParamName(i));
                    break;
                case STRING:
                case CHAR:
                    out.printf("%s = raw_input()%n",
                        method.getParamName(i));
                    break;
                case STRING_ARRAY:
                    out.printf("%sSize_ = int(raw_input())%n",
                        method.getParamName(i));
                    out.printf(TAB + "%1$s = [raw_input() for i in xrange(%1$sSize_)]%n",
                        method.getParamName(i));
                    break;
                }
            }

            out.println(TAB);
            out.print(TAB);
            if (method.getReturnType() != Type.VOID)
            {
                out.print("result_ = ");
            }
            out.printf("%s.%s(", genInstanceName(), method.getName());
            for (int i = 0; i < c; i++)
            {
                if (i > 0) out.print(", ");
                out.print(method.getParamName(i));
            }
            out.println(")");
            switch (method.getReturnType())
            {
            case INT:
            case LONG:
            case DOUBLE:
            case STRING:
            case CHAR:
                out.println(TAB);
                out.println(TAB + "print result_");
                out.println(TAB + "stdout.flush()");
                break;
            case INT_ARRAY:
            case LONG_ARRAY:
            case DOUBLE_ARRAY:
            case STRING_ARRAY:
                out.println(TAB);
                out.println(TAB + "print len(result_)");
                out.println(TAB + "for it_ in result_:");
                out.println(TAB + TAB + "print it_");
                out.println(TAB + "stdout.flush()");
                break;
            }

            out.println(TAB);
            out.println(TAB + "return 0");
        }

        // Main
        out.println();
        out.println("if __name__ == '__main__':");
        out.println(TAB);


        out.printf(TAB + "%2$s = %1$s()%n",
            info.getClassName(),
            genInstanceName());

        out.println(TAB);
        out.println(TAB + "# do edit codes if you need");

        for (Method method : info.getMethods())
        {
            out.println(TAB);
            out.printf(TAB + "%s(%s)%n",
                genCallerName(method.getName()),
                genInstanceName());
        }

        // End of Main

        // End of Class
    }

    String getDefaultValue(Type type)
    {
        switch (type)
        {
        case INT:
            return "0";
        case LONG:
            return "0L";
        case DOUBLE:
            return "0.0";
        case STRING:
            return "''";
        case INT_ARRAY:
        case LONG_ARRAY:
        case DOUBLE_ARRAY:
        case STRING_ARRAY:
            return "[]";
        case CHAR:
            return "'\\0'";
        case BOOL:
            return "False";
        default:
            throw new IllegalArgumentException("type: " + type);
        }
    }

    String genCallerName(String name)
    {
        return "call_" + name;
    }

    String genInstanceName()
    {
        String name = info.getClassName();
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }
}