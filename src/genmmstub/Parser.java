package genmmstub;

import java.util.*;

abstract class Parser
{
    static Parser newInstance(String lang)
    {
        switch (lang.toLowerCase())
        {
        case "java":
            return new JavaParser();
        case "c++":
        case "cpp":
            return new CppParser();
        case "c#":
        case "cs":
        case "csharp":
            return new CSharpParser();
        case "python":
        case "python2":
        case "py":
        case "py2":
            return new PythonParser();
        case "vb":
        case "vb.net":
        case "visualbasic":
        case "visualbasic.net":
        case "basic":
            return new VBParser();
        default:
            throw new IllegalArgumentException("lang: " + lang);
        }
    }

    private String className = "";
    private List<Method> methods = new ArrayList<>();
    private ImplMethod tempMethod = null;

    protected Parser()
    {
    }

    abstract void parse(String line);
    abstract Type parseType(String token);

    public ClassInfo getClassInfo()
    {
        if (tempMethod != null)
        {
            methods.add(tempMethod);
            tempMethod = null;
        }
        return new ClassInfo() {
            String _className = className;
            Method[] _methods = methods.toArray(new Method[0]);
            public String getClassName()
            {
                return _className;
            }
            public Method[] getMethods()
            {
                return _methods;
            }
            @Override
            public String toString()
            {
                StringBuilder sb = new StringBuilder();

                sb.append("{ class: " + getClassName() + ", methods: [");

                for (Method method : getMethods())
                {
                    sb.append(method.toString());
                    sb.append(", ");
                }

                sb.append(" ]}");
                return sb.toString();
            }
        };
    }

    protected void setClassName(String className)
    {
        this.className = className.trim();
    }

    protected void setNewMethod(String methodName)
    {
        if (tempMethod != null)
        {
            methods.add(tempMethod);
        }
        tempMethod = new ImplMethod();
        tempMethod.name = methodName.trim();
    }

    protected void setReturnType(Type type)
    {
        tempMethod.returnType = type;
    }
    protected void setReturnType(String token)
    {
        setReturnType(parseType(token.trim()));
    }

    protected void addParamType(Type type)
    {
        tempMethod.paramTypes.add(type);
    }
    protected void addParamType(String token)
    {
        addParamType(parseType(token.trim()));
    }

    protected void addParamName(String name)
    {
        tempMethod.paramNames.add(name.trim());
    }

    @Override
    public String toString()
    {
        return getClassInfo().toString();
    }

    class ImplMethod implements Method
    {
        String name = "";
        List<Type> paramTypes = new ArrayList<>();
        List<String> paramNames = new ArrayList<>();
        Type returnType = Type.VOID;

        public String getName()
        {
            return name;
        }

        public int paramCount()
        {
            return Math.min(paramTypes.size(), paramNames.size());
        }

        public String getParamName(int i)
        {
            return paramNames.get(i);
        }

        public Type getParamType(int i)
        {
            return paramTypes.get(i);
        }

        public Type getReturnType()
        {
            return returnType;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder();

            sb.append("{ name: " + getName() + ", params: [");

            int c = paramCount();
            for (int i = 0; i < c; i++)
            {
                sb.append("{ name: " + paramNames.get(i)
                    + ", type: " + paramTypes.get(i).toString()
                    + " }, ");
            }

            sb.append("], return: " + returnType.toString() + " }");

            return sb.toString();
        }
    }

}
