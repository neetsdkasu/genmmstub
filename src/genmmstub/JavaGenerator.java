package genmmstub;

import java.io.PrintStream;

class JavaGenerator extends Generator
{
    static final String TAB = "    ";

    @Override
    public String getExtension()
    {
        return ".java";
    }

    @Override
    public void generateCodeTo(PrintStream out)
    {
        out.println("import java.io.*;");
        out.println("import java.util.*;");
        out.println();

        out.printf("public class %s {%n", info.getClassName());

        for (Method method : info.getMethods())
        {
            out.println();
            out.printf(TAB + "public %s %s(",
                getTypeName(method.getReturnType()),
                method.getName());
            int c = method.paramCount();
            for (int i = 0; i < c; i++)
            {
                if (i > 0) out.print(", ");
                out.printf("%s %s",
                    getTypeName(method.getParamType(i)),
                    method.getParamName(i));
            }
            out.println(") {");
            if (method.getReturnType() != Type.VOID)
            {
                out.printf(TAB + TAB + "return %s;%n",
                    getDefaultValue(method.getReturnType()));
            }
            out.println(TAB + "}");
        }

        out.println();
        out.println(TAB + "// " + CUT_LINE);


        // Method Callers
        for (Method method : info.getMethods())
        {
            out.println();
            out.printf(TAB + "static int %s(BufferedReader in, %s %s) throws Exception {%n",
                genCallerName(method.getName()),
                info.getClassName(),
                genInstanceName());
            out.println();

            int c = method.paramCount();
            for (int i = 0; i < c; i++)
            {
                out.print(TAB + TAB);
                switch (method.getParamType(i))
                {
                case INT:
                    out.printf("int %s = Integer.parseInt(in.readLine());%n",
                        method.getParamName(i));
                    break;
                case INT_ARRAY:
                    out.printf("int _%sSize = Integer.parseInt(in.readLine());%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "int[] %1$s = new int[_%1$sSize];%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "for (int _idx = 0; _idx < _%sSize; _idx++) {%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + TAB + "%s[_idx] = Integer.parseInt(in.readLine());%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "}");
                    break;
                case LONG:
                    out.printf("long %s = Long.parseLong(in.readLine());%n",
                        method.getParamName(i));
                    break;
                case LONG_ARRAY:
                    out.printf("int _%sSize = Integer.parseInt(in.readLine());%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "long[] %1$s = new long[_%1$sSize];%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "for (int _idx = 0; _idx < _%sSize; _idx++) {%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + TAB + "%s[_idx] = Long.parseLong(in.readLine());%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "}");
                    break;
                case DOUBLE:
                    out.printf("double %s = Double.parseDouble(in.readLine());%n",
                        method.getParamName(i));
                    break;
                case DOUBLE_ARRAY:
                    out.printf("int _%sSize = Integer.parseInt(in.readLine());%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "double[] %1$s = new double[_%1$sSize];%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "for (int _idx = 0; _idx < _%sSize; _idx++) {%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + TAB + "%s[_idx] = Double.parseDouble(in.readLine());%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "}");
                    break;
                case STRING:
                    out.printf("String %s = in.readLine();%n",
                        method.getParamName(i));
                    break;
                case STRING_ARRAY:
                    out.printf("int _%sSize = Integer.parseInt(in.readLine());%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "String[] %1$s = new String[_%1$sSize];%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "for (int _idx = 0; _idx < _%sSize; _idx++) {%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + TAB + "%s[_idx] = in.readLine();%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "}");
                    break;
                case CHAR:
                    out.printf("char %s = in.readLine().charAt(0);%n",
                        method.getParamName(i));
                    break;
                
                }
            }

            out.println();
            out.print(TAB + TAB);
            if (method.getReturnType() != Type.VOID)
            {
                out.printf("%s _result = ", getTypeName(method.getReturnType()));
            }
            out.printf("%s.%s(", genInstanceName(), method.getName());
            for (int i = 0; i < c; i++)
            {
                if (i > 0) out.print(", ");
                out.print(method.getParamName(i));
            }
            out.println(");");
            switch (method.getReturnType())
            {
            case INT:
            case LONG:
            case STRING:
            case DOUBLE:
            case CHAR:
                out.println();
                out.println(TAB + TAB + "System.out.println(_result);");
                out.println(TAB + TAB + "System.out.flush();");
                break;
            case INT_ARRAY:
            case LONG_ARRAY:
            case DOUBLE_ARRAY:
            case STRING_ARRAY:
                String typeName = getTypeName(method.getReturnType());
                out.println();
                out.println(TAB + TAB + "System.out.println(_result.length);");
                out.printf(TAB + TAB + "for (%s _it : _result) {%n",
                    typeName.substring(0, typeName.indexOf('[')));
                out.println(TAB + TAB + TAB + "System.out.println(_it);");
                out.println(TAB + TAB + "}");
                out.println(TAB + TAB + "System.out.flush();");
                break;
            }

            out.println();
            out.println(TAB + TAB + "return 0;");
            out.println(TAB + "}");
        }

        // Main
        out.println();
        out.println(TAB + "public static void main(String[] args) throws Exception {");
        out.println();

        out.println(TAB + TAB
            + "BufferedReader in = new BufferedReader(new InputStreamReader(System.in));");

        out.printf(TAB + TAB + "%1$s %2$s = new %1$s();%n",
            info.getClassName(),
            genInstanceName());

        out.println();
        out.println(TAB + TAB + "// do edit codes if you need");

        for (Method method : info.getMethods())
        {
            out.println();
            out.printf(TAB + TAB + "%s(in, %s);%n",
                genCallerName(method.getName()),
                genInstanceName());
        }

        out.println();
        out.println(TAB + "}"); // End of Main

        out.println();
        out.println("}"); // End of Class
    }

    String getTypeName(Type type)
    {
        switch (type)
        {
        case INT:
            return "int";
        case INT_ARRAY:
            return "int[]";
        case LONG:
            return "long";
        case LONG_ARRAY:
            return "long[]";
        case DOUBLE:
            return "double";
        case DOUBLE_ARRAY:
            return "double[]";
        case STRING:
            return "String";
        case STRING_ARRAY:
            return "String[]";
        case VOID:
            return "void";
        case CHAR:
            return "char";
        case BOOL:
            return "boolean";
        default:
            throw new IllegalArgumentException("type: " + type);
        }
    }

    String getDefaultValue(Type type)
    {
        switch (type)
        {
        case INT:
            return "0";
        case INT_ARRAY:
            return "new int[0]";
        case LONG:
            return "0L";
        case LONG_ARRAY:
            return "new long[0]";
        case DOUBLE:
            return "0.0";
        case DOUBLE_ARRAY:
            return "new double[0]";
        case STRING:
            return "\"\"";
        case STRING_ARRAY:
            return "new String[0]";
        case CHAR:
            return "'A'";
        case BOOL:
            return "false";
        default:
            throw new IllegalArgumentException("type: " + type);
        }
    }

    String genCallerName(String name)
    {
        return "call" + name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    String genInstanceName()
    {
        String name = info.getClassName();
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }
}