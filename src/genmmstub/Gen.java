package genmmstub;

import java.io.*;
import java.util.*;

public class Gen
{
    public static void main(String[] args) throws Exception
    {
        String defLang = null;
        String outLang = "java";
        FileReader source = null;

        for (int i = 0; i < args.length; i++)
        {
            switch (args[i])
            {
            case "-def":
                defLang = args[++i];
                break;
            case "-out":
                outLang = args[++i];
                break;
            case "-f":
                if (source != null)
                {
                    source.close();
                }
                source = new FileReader(args[++i]);
                break;
            }
        }

        try
        {
            run(defLang, outLang, source);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (source != null)
            {
                source.close();
            }
        }
    }
    
    static Parser parseAuto(List<String> lines)
    {
        String[] langList = {"java", "c++", "c#", "python", "vb"};
        for (String lang : langList)
        {
            try
            {
                Parser parser = Parser.newInstance(lang);
                for (String line : lines)
                {
                    parser.parse(line);
                }
                return parser;
            }
            catch (IllegalArgumentException ex)
            {
                // no code
            }
        }
        throw new RuntimeException("Definition format is wrong!");
    }
    
    static void run(String defLang, String outLang, Reader _source) throws Exception
    {
        Parser parser = null;
        Generator generator = Generator.newInstance(outLang);
        Reader source = _source;
        
        if (defLang != null && !"auto".equals(defLang.toLowerCase()))
        {
            parser = Parser.newInstance(defLang);
        }
        
        if (source == null)
        {
            source = new InputStreamReader(System.in);
        }

        BufferedReader in = new BufferedReader(source);

        String lastInput = null, input;
        
        if (_source == null)
        {
            System.out.println("paste Definition");
        }
        
        List<String> lines = new ArrayList<>();

        while ((input = in.readLine()) != null)
        {
            if ("".equals(lastInput) && "".equals(input))
            {
                break;
            }
            input = input.trim();
            lines.add(input);
            lastInput = input;
        }
        
        if (parser != null)
        {
            for (String line : lines)
            {
                parser.parse(line);
            }
        }
        else
        {
            parser = parseAuto(lines);
        }

        generator.setClassInfo(parser.getClassInfo());

        File file = new File(generator.generateFileName());
        
        if (_source != null)
        {
            in = new BufferedReader(new InputStreamReader(System.in));
        }
        
        while (file.exists())
        {
            System.out.print("Already " + file.getName() + " exists. overwrite? [yes/no/newname] ");
            input = in.readLine();
            if ("".equals(input)) continue;
            if (input == null || "no".startsWith(input.toLowerCase()))
            {
                System.out.println("exit this program...");
                return;
            }
            if ("yes".startsWith(input.toLowerCase())) break;
            file = new File(generator.concatExtension(input));
        }

        try (PrintStream out = new PrintStream(file))
        {
            generator.generateCodeTo(out);
        }

        System.out.println("generate: " + file.getName());
    }
}
