package genmmstub;

import java.io.PrintStream;

class VBGenerator extends Generator
{
    static final String TAB = "    ";

    @Override
    public String getExtension()
    {
        return ".vb";
    }

    @Override
    public void generateCodeTo(PrintStream out)
    {
        out.println("Imports System");
        out.println("Imports System.Collections.Generic");
        out.println();

        out.printf("Public Class %s%n", info.getClassName());
        out.println();

        for (Method method : info.getMethods())
        {
            out.println();
            out.printf(TAB + "Public %s %s(",
                getMethodType(method.getReturnType()),
                method.getName());
            int c = method.paramCount();
            for (int i = 0; i < c; i++)
            {
                if (i > 0) out.print(", ");
                out.printf("%s As %s",
                    method.getParamName(i),
                    getTypeName(method.getParamType(i)));
            }
            if (method.getReturnType() != Type.VOID)
            {
                out.printf(") As %s%n", getTypeName(method.getReturnType()));
                out.println(TAB);
                out.printf(TAB + TAB + "Return %s%n",
                    getDefaultValue(method.getReturnType()));
            }
            else
            {
                out.println(")");
                out.println(TAB);
            }
            out.printf(TAB + "End %s%n", getMethodType(method.getReturnType()));
        }

        out.println();
        out.println(TAB + "' " + CUT_LINE);

        // Method Callers
        for (Method method : info.getMethods())
        {
            out.println();
            out.printf(TAB + "Shared Function %s(%s As %s) As Integer%n",
                genCallerName(method.getName()),
                genInstanceName(),
                info.getClassName());

            int c = method.paramCount();
            for (int i = 0; i < c; i++)
            {
                out.print(TAB + TAB);
                switch (method.getParamType(i))
                {
                case INT:
                    out.printf("Dim %s As Integer = CInt(Console.ReadLine())%n",
                        method.getParamName(i));
                    break;
                case INT_ARRAY:
                    out.printf("Dim _%sSize As Integer = CInt(Console.ReadLine()) - 1%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "Dim %1$s(_%1$sSize) As Integer%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "For _idx As Integer = 0 To _%sSize%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + TAB + "%s(_idx) = CInt(Console.ReadLine())%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "Next _idx");
                    break;
                case LONG:
                    out.printf("Dim %s As Long = CLng(Console.ReadLine())%n",
                        method.getParamName(i));
                    break;
                case LONG_ARRAY:
                    out.printf("Dim _%sSize As Integer = CInt(Console.ReadLine()) - 1%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "Dim %1$s(_%1$sSize) As Long%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "For _idx As Integer = 0 To _%sSize%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + TAB + "%s(_idx) = CLng(Console.ReadLine())%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "Next _idx");
                    break;
                case DOUBLE:
                    out.printf("Dim %s As Double = CDbl(Console.ReadLine())%n",
                        method.getParamName(i));
                    break;
                case DOUBLE_ARRAY:
                    out.printf("Dim _%sSize As Integer = CInt(Console.ReadLine()) - 1%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "Dim %1$s(_%1$sSize) As Double%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "For _idx As Integer = 0 To _%sSize%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + TAB + "%s(_idx) = CDbl(Console.ReadLine())%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "Next _idx");
                    break;
                case STRING:
                    out.printf("Dim %s As String = Console.ReadLine()%n",
                        method.getParamName(i));
                    break;
                case STRING_ARRAY:
                    out.printf("Dim _%sSize As Integer = CInt(Console.ReadLine()) - 1%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "Dim %1$s(_%1$sSize) As String%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "For _idx As Integer = 0 To _%sSize%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + TAB + "%s(_idx) = Console.ReadLine()%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "Next _idx");
                    break;
                case CHAR:
                    out.printf("Dim %s As Char = Console.ReadLine()(0)%n",
                        method.getParamName(i));
                    break;
                }
            }

            out.println();
            out.print(TAB + TAB);
            if (method.getReturnType() != Type.VOID)
            {
                out.printf("Dim _result As %s = ", getTypeName(method.getReturnType()));
            }
            out.printf("%s.%s(", genInstanceName(), method.getName());
            for (int i = 0; i < c; i++)
            {
                if (i > 0) out.print(", ");
                out.print(method.getParamName(i));
            }
            out.println(")");
            switch (method.getReturnType())
            {
            case INT:
            case LONG:
            case DOUBLE:
            case STRING:
            case CHAR:
                out.println();
                out.println(TAB + TAB + "Console.WriteLine(_result)");
                out.println(TAB + TAB + "Console.Out.Flush()");
                break;
            case INT_ARRAY:
            case LONG_ARRAY:
            case DOUBLE_ARRAY:
            case STRING_ARRAY:
                String typeName = getTypeName(method.getReturnType());
                out.println();
                out.println(TAB + TAB + "Console.WriteLine(_result.Length)");
                out.printf(TAB + TAB + "For Each _it As %s In _result%n",
                    typeName.substring(0, typeName.indexOf('(')));
                out.println(TAB + TAB + TAB + "Console.WriteLine(_it)");
                out.println(TAB + TAB + "Next _it");
                out.println(TAB + TAB + "Console.Out.Flush()");
                break;
            }

            out.println();
            out.println(TAB + TAB + "Return 0");
            out.println(TAB + "End Function");
        }

        // Main
        out.println();
        out.println(TAB + "Public Shared Sub Main()");

        out.printf(TAB + TAB + "Dim %2$s As New %1$s()%n",
            info.getClassName(),
            genInstanceName());

        out.println();
        out.println(TAB + TAB + "' do edit codes if you need");

        for (Method method : info.getMethods())
        {
            out.println();
            out.printf(TAB + TAB + "%s(%s)%n",
                genCallerName(method.getName()),
                genInstanceName());
        }

        out.println();
        out.println(TAB + "End Sub"); // End of Main

        out.println();
        out.println("End Class"); // End of Class
    }

    String getMethodType(Type type)
    {
        return type == Type.VOID ? "Sub" : "Function";
    }

    String getTypeName(Type type)
    {
        switch (type)
        {
        case INT:
            return "Integer";
        case INT_ARRAY:
            return "Integer()";
        case LONG:
            return "Long";
        case LONG_ARRAY:
            return "Long()";
        case DOUBLE:
            return "Double";
        case DOUBLE_ARRAY:
            return "Double()";
        case STRING:
            return "String";
        case STRING_ARRAY:
            return "String()";
        case CHAR:
            return "Char";
        case BOOL:
            return "Boolean";
        default:
            throw new IllegalArgumentException("type: " + type);
        }
    }

    String getDefaultValue(Type type)
    {
        switch (type)
        {
        case INT:
            return "0";
        case INT_ARRAY:
            return "New Integer(){}";
        case LONG:
            return "0&";
        case LONG_ARRAY:
            return "New Long(){}";
        case DOUBLE:
            return "0.0";
        case DOUBLE_ARRAY:
            return "New Double(){}";
        case STRING:
            return "\"\"";
        case STRING_ARRAY:
            return "New String(){}";
        case CHAR:
            return "\"A\"c";
        case BOOL:
            return "False";
        default:
            throw new IllegalArgumentException("type: " + type);
        }
    }

    String genCallerName(String name)
    {
        return "Call" + name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    String genInstanceName()
    {
        String name = info.getClassName();
        return "_" + name.substring(0, 1).toLowerCase() + name.substring(1);
    }
}