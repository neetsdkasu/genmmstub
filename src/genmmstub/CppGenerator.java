package genmmstub;

import java.io.PrintStream;

class CppGenerator extends Generator
{
    static final String TAB = "    ";

    @Override
    public String getExtension()
    {
        return ".cpp";
    }

    @Override
    public void generateCodeTo(PrintStream out)
    {
        out.println("#include <iostream>");
        out.println("#include <string>");
        out.println("#include <vector>");
        out.println();
        out.println("using namespace std;");
        out.println();

        out.printf("class %s {%n", info.getClassName());
        out.println("public:");

        for (Method method : info.getMethods())
        {
            out.println();
            out.printf(TAB + "%s %s(",
                getTypeName(method.getReturnType()),
                method.getName());
            int c = method.paramCount();
            for (int i = 0; i < c; i++)
            {
                if (i > 0) out.print(", ");
                out.printf("%s %s",
                    getTypeName(method.getParamType(i)),
                    method.getParamName(i));
            }
            out.println(") {");
            switch (method.getReturnType())
            {
            case INT:
            case LONG:
            case DOUBLE:
            case CHAR:
            case BOOL:
                out.printf(TAB + TAB + "return %s;%n",
                    getDefaultValue(method.getReturnType()));
                break;
            case STRING:
                out.println(TAB + TAB + "string ret = \"\";");
                out.println(TAB + TAB + "return ret;");
                break;
            case INT_ARRAY:
            case LONG_ARRAY:
            case DOUBLE_ARRAY:
            case STRING_ARRAY:
                out.printf(TAB + TAB + "%s ret;%n", getTypeName(method.getReturnType()));
                out.println(TAB + TAB + "return ret;");
                break;
            }
            out.println(TAB + "}");
        }

        out.println();
        out.println("};");

        out.println();
        out.println("// " + CUT_LINE);

        // Method Callers
        for (Method method : info.getMethods())
        {
            out.println();
            out.printf("int %s(%s &%s) {%n",
                genCallerName(method.getName()),
                info.getClassName(),
                genInstanceName());
            out.println();

            out.println(TAB + "string _buf;");
            out.println();

            int c = method.paramCount();
            for (int i = 0; i < c; i++)
            {
                out.print(TAB);
                switch (method.getParamType(i))
                {
                case INT:
                    out.println("getline(cin, _buf);");
                    out.printf(TAB + "int %s = stoi(_buf, nullptr);%n",
                        method.getParamName(i));
                    break;
                case INT_ARRAY:
                    out.println("getline(cin, _buf);");
                    out.printf(TAB + "int _%sSize = stoi(_buf, nullptr);%n",
                        method.getParamName(i));
                    out.printf(TAB + "vector<int> %1$s(_%1$sSize);%n",
                        method.getParamName(i));
                    out.printf(TAB + "for (int _idx = 0; _idx < _%sSize; _idx++) {%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "getline(cin, _buf);");
                    out.printf(TAB + TAB + "%s[_idx] = stoi(_buf, nullptr);%n",
                        method.getParamName(i));
                    out.println(TAB + "}");
                    break;
                case LONG:
                    out.println("getline(cin, _buf);");
                    out.printf(TAB + "long long %s = stoll(_buf, nullptr);%n",
                        method.getParamName(i));
                    break;
                case LONG_ARRAY:
                    out.println("getline(cin, _buf);");
                    out.printf(TAB + "int _%sSize = stoi(_buf, nullptr);%n",
                        method.getParamName(i));
                    out.printf(TAB + "vector<long long> %1$s(_%1$sSize);%n",
                        method.getParamName(i));
                    out.printf(TAB + "for (int _idx = 0; _idx < _%sSize; _idx++) {%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "getline(cin, _buf);");
                    out.printf(TAB + TAB + "%s[_idx] = stoll(_buf, nullptr);%n",
                        method.getParamName(i));
                    out.println(TAB + "}");
                    break;
                case DOUBLE:
                    out.println("getline(cin, _buf);");
                    out.printf(TAB + "double %s = stod(_buf, nullptr);%n",
                        method.getParamName(i));
                    break;
                case DOUBLE_ARRAY:
                    out.println("getline(cin, _buf);");
                    out.printf(TAB + "int _%sSize = stoi(_buf, nullptr);%n",
                        method.getParamName(i));
                    out.printf(TAB + "vector<double> %1$s(_%1$sSize);%n",
                        method.getParamName(i));
                    out.printf(TAB + "for (int _idx = 0; _idx < _%sSize; _idx++) {%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "getline(cin, _buf);");
                    out.printf(TAB + TAB + "%s[_idx] = stod(_buf, nullptr);%n",
                        method.getParamName(i));
                    out.println(TAB + "}");
                    break;
                case STRING:
                    out.printf("string %1$s;%n" + TAB + "getline(cin, %1$s);%n",
                        method.getParamName(i));
                    break;
                case STRING_ARRAY:
                    out.println("getline(cin, _buf);");
                    out.printf(TAB + "int _%sSize = stoi(_buf, nullptr);%n",
                        method.getParamName(i));
                    out.printf(TAB + "vector<string> %1$s(_%1$sSize);%n",
                        method.getParamName(i));
                    out.printf(TAB + "for (int _idx = 0; _idx < _%sSize; _idx++) {%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "getline(cin, %s[_idx]);%n",
                        method.getParamName(i));
                    out.println(TAB + "}");
                    break;
                case CHAR:
                    out.println("getline(cin, _buf);");
                    out.printf(TAB + "char %s = _buf[0];%n",
                        method.getParamName(i));
                    break;
                }
            }

            out.println();
            out.print(TAB);
            if (method.getReturnType() != Type.VOID)
            {
                out.printf("%s _result = ", getTypeName(method.getReturnType()));
            }
            out.printf("%s.%s(", genInstanceName(), method.getName());
            for (int i = 0; i < c; i++)
            {
                if (i > 0) out.print(", ");
                out.print(method.getParamName(i));
            }
            out.println(");");
            switch (method.getReturnType())
            {
            case INT:
            case LONG:
            case DOUBLE:
            case STRING:
            case CHAR:
                out.println();
                out.println(TAB + "cout << _result << endl;");
                out.println(TAB + "cout.flush();");
                break;
            case INT_ARRAY:
            case LONG_ARRAY:
            case DOUBLE_ARRAY:
            case STRING_ARRAY:
                String typeName = getTypeName(method.getReturnType());
                out.println();
                out.println(TAB + "cout << _result.size() << endl;");
                out.printf(TAB + "for (%s& _it : _result) {%n",
                    typeName.substring(typeName.indexOf('<') + 1, typeName.indexOf('>')));
                out.println(TAB + TAB + "cout << _it << endl;");
                out.println(TAB + "}");
                out.println(TAB + "cout.flush();");
                break;
            }

            out.println();
            out.println(TAB + "return 0;");
            out.println("}");
        }

        // Main
        out.println();
        out.println("int main() {");
        out.println();

        out.printf(TAB + "%s %s;%n",
            info.getClassName(),
            genInstanceName());

        out.println();
        out.println(TAB + "// do edit codes if you need");

        for (Method method : info.getMethods())
        {
            out.println();
            out.printf(TAB + "%s(%s);%n",
                genCallerName(method.getName()),
                genInstanceName());
        }

        out.println();
        out.println(TAB + "return 0;");
        out.println("}"); // End of Main
    }

    String getTypeName(Type type)
    {
        switch (type)
        {
        case INT:
            return "int";
        case INT_ARRAY:
            return "vector<int>";
        case LONG:
            return "long long";
        case LONG_ARRAY:
            return "vector<long long>";
        case DOUBLE:
            return "double";
        case DOUBLE_ARRAY:
            return "vector<double>";
        case STRING:
            return "string";
        case STRING_ARRAY:
            return "vector<string>";
        case VOID:
            return "void";
        case CHAR:
            return "char";
        case BOOL:
            return "bool";
        default:
            throw new IllegalArgumentException("type: " + type);
        }
    }

    String getDefaultValue(Type type)
    {
        switch (type)
        {
        case INT:
            return "0";
        case LONG:
            return "0LL";
        case DOUBLE:
            return "0.0";
        case STRING:
            return "\"\"";
        case INT_ARRAY:
        case LONG_ARRAY:
        case DOUBLE_ARRAY:
        case STRING_ARRAY:
            return "{}";
        case CHAR:
            return "'A'";
        case BOOL:
            return "false";
        default:
            throw new IllegalArgumentException("type: " + type);
        }
    }
    
    String genCallerName(String name)
    {
        return "call_" + name;
    }

    String genInstanceName()
    {
        String name = info.getClassName();
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }
}