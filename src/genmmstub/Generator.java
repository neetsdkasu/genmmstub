package genmmstub;

import java.io.PrintStream;

abstract class Generator
{
    static Generator newInstance(String lang)
    {
        switch (lang.toLowerCase())
        {
        case "java":
            return new JavaGenerator();
        case "c++":
        case "cpp":
            return new CppGenerator();
        case "c#":
        case "cs":
        case "csharp":
            return new CSharpGenerator();
        case "python":
        case "python2":
        case "py":
        case "py2":
            return new PythonGenerator();
        case "vb":
        case "vb.net":
        case "visualbasic":
        case "visualbasic.net":
        case "basic":
            return new VBGenerator();
        default:
            throw new IllegalArgumentException("lang: " + lang);
        }
    }

    static final String LN = System.lineSeparator();
    static final String CUT_LINE = "-------8<------- end of solution submitted to the website -------8<-------";

    protected ClassInfo info = null;

    abstract String getExtension();
    abstract void generateCodeTo(PrintStream out);

    public void setClassInfo(ClassInfo info)
    {
        this.info = info;
    }

    public String concatExtension(String name)
    {
        if (name.endsWith(getExtension())) return name;
        return name + getExtension();
    }

    public String generateFileName()
    {
        return concatExtension(info.getClassName());
    }
}