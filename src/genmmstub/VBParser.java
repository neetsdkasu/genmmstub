package genmmstub;

class VBParser extends Parser
{
    @Override
    public void parse(String line)
    {
        line = line.trim();
        int pos = line.indexOf(':');
        if (pos < 0) return;

        String kind = line.substring(0, pos);
        String text = line.substring(pos + 1).trim();

        switch (kind)
        {
        case "Class":
            setClassName(text);
            break;
        case "Method":
            setNewMethod(text);
            break;
        case "Returns":
            setReturnType(text);
            break;
        case "Parameters":
            for (String token : text.split(","))
            {
                addParamType(token);
            }
            break;
        case "Method signature":
            text = text.substring(text.indexOf('(') + 1);
            int ob = text.lastIndexOf('(');
            int cb = text.lastIndexOf(')');
            if (ob >= 0 && ob + 1 == cb)
            {
                text = text.substring(0, ob);
                cb = text.lastIndexOf(')');
            }
            text = text.substring(0, cb);
            for (String param : text.split(","))
            {
                addParamName(param.trim().split(" ")[0]);
            }
            break;
        }
    }

    @Override
    public Type parseType(String token)
    {
        token = token.trim();
        if ("".equals(token)) return Type.VOID;
        switch (token)
        {
        case "Integer":
            return Type.INT;
        case "Integer()":
            return Type.INT_ARRAY;
        case "Long":
            return Type.LONG;
        case "Long()":
            return Type.LONG_ARRAY;
        case "Double":
            return Type.DOUBLE;
        case "Double()":
            return Type.DOUBLE_ARRAY;
        case "String":
            return Type.STRING;
        case "String()":
            return Type.STRING_ARRAY;
        case "Char":
            return Type.CHAR;
        case "Boolean":
            return Type.BOOL;
        default:
            throw new IllegalArgumentException("token: " + token);
        }
    }
}

