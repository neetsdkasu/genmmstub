package genmmstub;

interface Method
{
    String getName();
    Type getReturnType();
    int paramCount();
    String getParamName(int i);
    Type getParamType(int i);
}