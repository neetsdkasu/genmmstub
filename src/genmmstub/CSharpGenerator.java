package genmmstub;

import java.io.PrintStream;

class CSharpGenerator extends Generator
{
    static final String TAB = "    ";

    @Override
    public String getExtension()
    {
        return ".cs";
    }

    @Override
    public void generateCodeTo(PrintStream out)
    {
        out.println("using System;");
        out.println("using System.Collections.Generic;");
        out.println();

        out.printf("public class %s%n", info.getClassName());
        out.println("{");

        for (Method method : info.getMethods())
        {
            out.println();
            out.printf(TAB + "public %s %s(",
                getTypeName(method.getReturnType()),
                method.getName());
            int c = method.paramCount();
            for (int i = 0; i < c; i++)
            {
                if (i > 0) out.print(", ");
                out.printf("%s %s",
                    getTypeName(method.getParamType(i)),
                    method.getParamName(i));
            }
            out.println(")");
            out.println(TAB + "{");
            if (method.getReturnType() != Type.VOID)
            {
                out.printf(TAB + TAB + "return %s;%n",
                    getDefaultValue(method.getReturnType()));
            }
            out.println(TAB + "}");
        }

        out.println();
        out.println(TAB + "// " + CUT_LINE);

        // Method Callers
        for (Method method : info.getMethods())
        {
            out.println();
            out.printf(TAB + "static int %s(%s %s)%n",
                genCallerName(method.getName()),
                info.getClassName(),
                genInstanceName());
            out.println(TAB + "{");

            int c = method.paramCount();
            for (int i = 0; i < c; i++)
            {
                out.print(TAB + TAB);
                switch (method.getParamType(i))
                {
                case INT:
                    out.printf("int %s = int.Parse(Console.ReadLine());%n",
                        method.getParamName(i));
                    break;
                case INT_ARRAY:
                    out.printf("int _%sSize = int.Parse(Console.ReadLine());%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "int[] %1$s = new int[_%1$sSize];%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "for (int _idx = 0; _idx < _%sSize; _idx++)%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "{");
                    out.printf(TAB + TAB + TAB + "%s[_idx] = int.Parse(Console.ReadLine());%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "}");
                    break;
                case LONG:
                    out.printf("long %s = long.Parse(Console.ReadLine());%n",
                        method.getParamName(i));
                    break;
                case LONG_ARRAY:
                    out.printf("int _%sSize = int.Parse(Console.ReadLine());%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "long[] %1$s = new long[_%1$sSize];%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "for (int _idx = 0; _idx < _%sSize; _idx++)%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "{");
                    out.printf(TAB + TAB + TAB + "%s[_idx] = long.Parse(Console.ReadLine());%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "}");
                    break;
                case DOUBLE:
                    out.printf("double %s = double.Parse(Console.ReadLine());%n",
                        method.getParamName(i));
                    break;
                case DOUBLE_ARRAY:
                    out.printf("int _%sSize = int.Parse(Console.ReadLine());%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "double[] %1$s = new double[_%1$sSize];%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "for (int _idx = 0; _idx < _%sSize; _idx++)%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "{");
                    out.printf(TAB + TAB + TAB + "%s[_idx] = double.Parse(Console.ReadLine());%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "}");
                    break;
                case STRING:
                    out.printf("string %s = Console.ReadLine();%n",
                        method.getParamName(i));
                    break;
                case STRING_ARRAY:
                    out.printf("int _%sSize = int.Parse(Console.ReadLine());%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "string[] %1$s = new string[_%1$sSize];%n",
                        method.getParamName(i));
                    out.printf(TAB + TAB + "for (int _idx = 0; _idx < _%sSize; _idx++)%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "{");
                    out.printf(TAB + TAB + TAB + "%s[_idx] = Console.ReadLine();%n",
                        method.getParamName(i));
                    out.println(TAB + TAB + "}");
                    break;
                case CHAR:
                    out.printf("char %s = Console.ReadLine()[0];%n",
                        method.getParamName(i));
                    break;
                }
            }

            out.println();
            out.print(TAB + TAB);
            if (method.getReturnType() != Type.VOID)
            {
                out.printf("%s _result = ", getTypeName(method.getReturnType()));
            }
            out.printf("%s.%s(", genInstanceName(), method.getName());
            for (int i = 0; i < c; i++)
            {
                if (i > 0) out.print(", ");
                out.print(method.getParamName(i));
            }
            out.println(");");
            switch (method.getReturnType())
            {
            case INT:
            case LONG:
            case DOUBLE:
            case STRING:
            case CHAR:
                out.println();
                out.println(TAB + TAB + "Console.WriteLine(_result);");
                out.println(TAB + TAB + "Console.Out.Flush();");
                break;
            case INT_ARRAY:
            case LONG_ARRAY:
            case DOUBLE_ARRAY:
            case STRING_ARRAY:
                String typeName = getTypeName(method.getReturnType());
                out.println();
                out.println(TAB + TAB + "Console.WriteLine(_result.Length);");
                out.printf(TAB + TAB + "foreach (%s _it in _result)%n",
                    typeName.substring(0, typeName.indexOf('[')));
                out.println(TAB + TAB + "{");
                out.println(TAB + TAB + TAB + "Console.WriteLine(_it);");
                out.println(TAB + TAB + "}");
                out.println(TAB + TAB + "Console.Out.Flush();");
                break;
            }

            out.println();
            out.println(TAB + TAB + "return 0;");
            out.println(TAB + "}");
        }

        // Main
        out.println();
        out.println(TAB + "public static void Main()");
        out.println(TAB + "{");

        out.printf(TAB + TAB + "%1$s %2$s = new %1$s();%n",
            info.getClassName(),
            genInstanceName());

        out.println();
        out.println(TAB + TAB + "// do edit codes if you need");

        for (Method method : info.getMethods())
        {
            out.println();
            out.printf(TAB + TAB + "%s(%s);%n",
                genCallerName(method.getName()),
                genInstanceName());
        }

        out.println();
        out.println(TAB + "}"); // End of Main

        out.println();
        out.println("}"); // End of Class
    }

    String getTypeName(Type type)
    {
        switch (type)
        {
        case INT:
            return "int";
        case INT_ARRAY:
            return "int[]";
        case LONG:
            return "long";
        case LONG_ARRAY:
            return "long[]";
        case DOUBLE:
            return "double";
        case DOUBLE_ARRAY:
            return "double[]";
        case STRING:
            return "string";
        case STRING_ARRAY:
            return "string[]";
        case VOID:
            return "void";
        case CHAR:
            return "char";
        case BOOL:
            return "bool";
        default:
            throw new IllegalArgumentException("type: " + type);
        }
    }

    String getDefaultValue(Type type)
    {
        switch (type)
        {
        case INT:
            return "0";
        case INT_ARRAY:
            return "new int[0]";
        case LONG:
            return "0L";
        case LONG_ARRAY:
            return "new long[0]";
        case DOUBLE:
            return "0.0";
        case DOUBLE_ARRAY:
            return "new double[0]";
        case STRING:
            return "\"\"";
        case STRING_ARRAY:
            return "new string[0]";
        case CHAR:
            return "'A'";
        case BOOL:
            return "false";
        default:
            throw new IllegalArgumentException("type: " + type);
        }
    }

    String genCallerName(String name)
    {
        return "Call" + name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    String genInstanceName()
    {
        String name = info.getClassName();
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }
}