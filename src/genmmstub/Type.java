package genmmstub;

enum Type
{
    INT,
    LONG,
    DOUBLE,
    STRING,
    INT_ARRAY,
    LONG_ARRAY,
    DOUBLE_ARRAY,
    STRING_ARRAY,
    VOID,
    CHAR,
    BOOL
}