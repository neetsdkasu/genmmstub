package genmmstub;

class JavaParser extends Parser
{
    @Override
    public void parse(String line)
    {
        line = line.trim();
        int pos = line.indexOf(':');
        if (pos < 0) return;

        String kind = line.substring(0, pos);
        String text = line.substring(pos + 1).trim();

        switch (kind)
        {
        case "Class":
            setClassName(text);
            break;
        case "Method":
            setNewMethod(text);
            break;
        case "Returns":
            setReturnType(text);
            break;
        case "Parameters":
            for (String token : text.split(","))
            {
                addParamType(token);
            }
            break;
        case "Method signature":
            text = text.substring(text.indexOf('(') + 1);
            text = text.substring(0, text.lastIndexOf(')'));
            for (String param : text.split(","))
            {
                addParamName(param.trim().split(" ")[1]);
            }
            break;
        }
    }

    @Override
    public Type parseType(String token)
    {
        token = token.trim();
        switch (token)
        {
        case "int":
            return Type.INT;
        case "int[]":
            return Type.INT_ARRAY;
        case "long":
            return Type.LONG;
        case "long[]":
            return Type.LONG_ARRAY;
        case "double":
            return Type.DOUBLE;
        case "double[]":
            return Type.DOUBLE_ARRAY;
        case "String":
            return Type.STRING;
        case "String[]":
            return Type.STRING_ARRAY;
        case "void":
            return Type.VOID;
        case "char":
            return Type.CHAR;
        case "boolean":
            return Type.BOOL;
        default:
            throw new IllegalArgumentException("token: " + token);
        }
    }
}

