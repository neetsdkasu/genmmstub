@echo off

if not exist Gen.jar ( goto nogenjar )

java -jar Gen.jar %*

goto endlabel


:nogenjar

if not exist bin ( goto notcurdir )
if not exist bin\Gen.jar ( goto notcurdir )

java -jar bin\Gen.jar %*

goto endlabel


:notcurdir

if not exist "%~dp0Gen.jar" ( goto notbatdir )

java -jar "%~dp0Gen.jar" %*

goto endlabel


:notbatdir

if not exist "%~dp0bin" ( goto notfound )
if not exist "%~dp0bin\Gen.jar" ( goto notfound )

java -jar "%~dp0bin\Gen.jar" %*

goto endlabel



:notfound
echo ERROR not found Gan.jar
exit /B 1
goto :EOF


:endlabel
