@pushd "%~dp0"
@setlocal

@set classdir=classes
@set srcdir=src
@set bindir=bin

@set srcfile=%srcdir%\genmmstub\Gen.java
@set jarpath=%bindir%\Gen.jar
@set entrypoint=genmmstub.Gen

@if not exist "%classdir%" ( mkdir "%classdir%" )

javac ^
    -encoding "utf8" ^
    -d "%classdir%" ^
    -sourcepath "%srcdir%" ^
    "%srcfile%"

@if ERRORLEVEL 1 ( goto endlabel )

@if not exist "%bindir%" ( mkdir "%bindir%" )

@if exist "%jarpath%" ( goto updatejar ) else ( goto makejar )


:makejar

jar cvfe "%jarpath%" %entrypoint% -C "%classdir%" .

@goto endlabel


:updatejar

jar uvfe "%jarpath%" %entrypoint% -C "%classdir%" .

@goto endlabel


:endlabel

@endlocal
@popd
