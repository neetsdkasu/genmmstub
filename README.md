genMMstub
=========

TCMMのProblem StatementのページのDefinition記述のコピペから雑な空ソースコードファイルを生成するだけ  
※自分用に作った  



### はじめに

WindowsでやったのでWindows前提で。  
コンパイルには`JDK7`か`JDK8`がインストールされてること前提。  
実行には`Java7(JRE7)`か`Java8(JRE8)`がインストールされてること前提。  
(Windowsに依存するJavaコード自体は入ってないハズなのでWindows以外でも動くはず)   
(Java9以降は環境持ってないためコンパイルや実行ができるかは未確認)  



### コンパイル

`compile.bat`を実行すると`bin`ディレクトリに`Gen.jar`が生成される。   
コンパイルの詳細を知りたいなら`compile.bat`を読んで  

コンパイル済みの`Gar.jar`ファイルは[Tagページ](https://gitlab.com/neetsdkasu/genmmstub/tags)のタグにアップロードしておく予定ではあるが  



### 実行

基本は`java -jar Gen.jar`で動く。  

なお、jarファイルの指定はパスで指定するため、  
例えば(windowsで)相対パスで`bin\Gen.jar`にファイルがあるなら`java -jar bin\Gen.jar`と指定しなけらばならない。  
(※windowsのパス区切りは`\`だがunix系なら`/`なので`java -jar bin/Gen.jar`となる)  


実行するとコンソールに`paste Definition`と表示されるので、  
表示されたらDefinitionをコンソールにコピペ、  
その後にエンターキー(リターンキー/改行キー)を2～3回くらい押すと空コードがファイルに書き出される。  


`java -jar Gen.jar` の場合はDefinitionからJavaの空コードを生成する。  
`java -jar Gen.jar -out java` の場合はDefinitionからJavaの空コードを生成する。  
`java -jar Gen.jar -out cpp` の場合はDefinitionからC++の空コードを生成する。  
`java -jar Gen.jar -out cs` の場合はDefinitionからC#の空コードを生成する。  
`java -jar Gen.jar -out py` の場合はDefinitionからPythonの空コードを生成する。  
`java -jar Gen.jar -out vb` の場合はDefinitionからVB.NETの空コードを生成する。  


Definitionの入力をコンソールへのコピペではなく  
Definitionをファイルに保存してから(例えばDefinition.txt)  
`java -jar Gen.jar -f Definition.txt` と指定すればファイルから読み込める  


もちろんフラグは組み合わせて使える  
`java -jar Gen.jar -f hoge.txt -out vb`  
この場合はDefinitionが記載されたhoge.txtからVB.NETの空コードを生成する。  



### 課題・目標・希望など
 
 - ライブラリクラスのこと忘れてた(これも標準入出力でオフラインテスターとやりとりするんだった…)  
 - 提出部(ソルバ部分)とオフラインテスターとの通信部とを別々のファイルで保存できるようにしたい  
 - テスター側のコード生成するのも作り入れたい(古い問題にはオフラインテスター無いものもあり…)  
 - TCにない言語のコードも生成できたら面白いか？(別に面白くもないか…)  
 - 生成ファイルのファイル名を指定できたほうがよいのか？   
 - 生成ファイルの出力先ディレクトリを指定できるようにしたいね   
 - メソッドの戻り値型をintにしているがやりとりしたデータ全部返すクラスにでもしたほうがいいのか？  
 


### 利用上の注意

オフラインテスターから複数回・複数種類のメソッドを呼び出すような問題のときは当該部分のコードを手作業で変更が必要になる。  

メソッドの引数が言語の予約済みや定義済みのキーワードの衝突を避けることが出来ない  

オフラインテスターとのやりとりの書式（プロトコル）が引数順に1行1値スタイルではない場合は生成コードを手直しする必要がある  

 

### Definitionの例

これをコピペする(各行の行頭・行末の空白くらいはtrim処理するので気にせずコピペ)  
Definitionの言語はJavaでもC++でもC#でもPythonでもVB.NETでもどれでもおｋ   
※この例のはJavaになってる  

[TCO17のMMR2](https://community.topcoder.com/longcontest/?module=ViewProblemStatement&rd=16966&compid=55902&popup=false&lid=4)のDefinitionより
```
Definition
    	
Class:	AbstractWars
Method:	init
Parameters:	int[], int
Returns:	int
Method signature:	int init(int[] baseLocations, int speed)
 
Method:	sendTroops
Parameters:	int[], int[]
Returns:	int[]
Method signature:	int[] sendTroops(int[] bases, int[] troops)
(be sure your methods are public)
```
※コロン区切りのない行は現状は無視される  



#### Definitionの例から生成したVB.NETコード

```vb
Imports System
Imports System.Collections.Generic

Public Class AbstractWars


    Public Function init(baseLocations As Integer(), speed As Integer) As Integer
    
        Return 0
    End Function

    Public Function sendTroops(bases As Integer(), troops As Integer()) As Integer()
    
        Return New Integer(){}
    End Function

    ' -------8<------- end of solution submitted to the website -------8<-------

    Shared Function CallInit(_abstractWars As AbstractWars) As Integer
        Dim _baseLocationsSize As Integer = CInt(Console.ReadLine()) - 1
        Dim baseLocations(_baseLocationsSize) As Integer
        For _idx As Integer = 0 To _baseLocationsSize
            baseLocations(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim speed As Integer = CInt(Console.ReadLine())

        Dim _result As Integer = _abstractWars.init(baseLocations, speed)

        Console.WriteLine(_result)
        Console.Out.Flush()

        Return 0
    End Function

    Shared Function CallSendTroops(_abstractWars As AbstractWars) As Integer
        Dim _basesSize As Integer = CInt(Console.ReadLine()) - 1
        Dim bases(_basesSize) As Integer
        For _idx As Integer = 0 To _basesSize
            bases(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim _troopsSize As Integer = CInt(Console.ReadLine()) - 1
        Dim troops(_troopsSize) As Integer
        For _idx As Integer = 0 To _troopsSize
            troops(_idx) = CInt(Console.ReadLine())
        Next _idx

        Dim _result As Integer() = _abstractWars.sendTroops(bases, troops)

        Console.WriteLine(_result.Length)
        For Each _it As Integer In _result
            Console.WriteLine(_it)
        Next _it
        Console.Out.Flush()

        Return 0
    End Function

    Public Shared Sub Main()
        Dim _abstractWars As New AbstractWars()

        ' do edit codes if you need

        CallInit(_abstractWars)

        CallSendTroops(_abstractWars)

    End Sub

End Class
```

VBはクラス名や変数名の大文字小文字を区別しないため  
メソッドの引数名や自動生成の変数名などに気をつけないといけないが  
現在のところ気をつけているのはDefinitionのクラス名のインスタンス名くらいしかないので   
今後の課題ではある…   
今んとこTCMMではSolution.vbとMain.vbにソルバと通信部を分けて毎回やってるので  
このgenMMstubでもファイル分割して名前も指定してソースファイル生成できるようにしたい…  




#### Definitionの例から生成したJavaコード

```java
import java.io.*;
import java.util.*;

public class AbstractWars {

    public int init(int[] baseLocations, int speed) {
        return 0;
    }

    public int[] sendTroops(int[] bases, int[] troops) {
        return new int[0];
    }

    // -------8<------- end of solution submitted to the website -------8<-------

    static int callInit(BufferedReader in, AbstractWars abstractWars) throws Exception {

        int _baseLocationsSize = Integer.parseInt(in.readLine());
        int[] baseLocations = new int[_baseLocationsSize];
        for (int _idx = 0; _idx < _baseLocationsSize; _idx++) {
            baseLocations[_idx] = Integer.parseInt(in.readLine());
        }
        int speed = Integer.parseInt(in.readLine());

        int _result = abstractWars.init(baseLocations, speed);

        System.out.println(_result);
        System.out.flush();

        return 0;
    }

    static int callSendTroops(BufferedReader in, AbstractWars abstractWars) throws Exception {

        int _basesSize = Integer.parseInt(in.readLine());
        int[] bases = new int[_basesSize];
        for (int _idx = 0; _idx < _basesSize; _idx++) {
            bases[_idx] = Integer.parseInt(in.readLine());
        }
        int _troopsSize = Integer.parseInt(in.readLine());
        int[] troops = new int[_troopsSize];
        for (int _idx = 0; _idx < _troopsSize; _idx++) {
            troops[_idx] = Integer.parseInt(in.readLine());
        }

        int[] _result = abstractWars.sendTroops(bases, troops);

        System.out.println(_result.length);
        for (int _it : _result) {
            System.out.println(_it);
        }
        System.out.flush();

        return 0;
    }

    public static void main(String[] args) throws Exception {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        AbstractWars abstractWars = new AbstractWars();

        // do edit codes if you need

        callInit(in, abstractWars);

        callSendTroops(in, abstractWars);

    }

}

```



#### Definitionの例から生成したC++コード

```c++
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class AbstractWars {
public:

    int init(vector<int> baseLocations, int speed) {
        return 0;
    }

    vector<int> sendTroops(vector<int> bases, vector<int> troops) {
        vector<int> ret;
        return ret;
    }

};

// -------8<------- end of solution submitted to the website -------8<-------

int call_init(AbstractWars &abstractWars) {

    string _buf;

    getline(cin, _buf);
    int _baseLocationsSize = stoi(_buf, nullptr);
    vector<int> baseLocations(_baseLocationsSize);
    for (int _idx = 0; _idx < _baseLocationsSize; _idx++) {
        getline(cin, _buf);
        baseLocations[_idx] = stoi(_buf, nullptr);
    }
    getline(cin, _buf);
    int speed = stoi(_buf, nullptr);

    int _result = abstractWars.init(baseLocations, speed);

    cout << _result << endl;
    cout.flush();

    return 0;
}

int call_sendTroops(AbstractWars &abstractWars) {

    string _buf;

    getline(cin, _buf);
    int _basesSize = stoi(_buf, nullptr);
    vector<int> bases(_basesSize);
    for (int _idx = 0; _idx < _basesSize; _idx++) {
        getline(cin, _buf);
        bases[_idx] = stoi(_buf, nullptr);
    }
    getline(cin, _buf);
    int _troopsSize = stoi(_buf, nullptr);
    vector<int> troops(_troopsSize);
    for (int _idx = 0; _idx < _troopsSize; _idx++) {
        getline(cin, _buf);
        troops[_idx] = stoi(_buf, nullptr);
    }

    vector<int> _result = abstractWars.sendTroops(bases, troops);

    cout << _result.size() << endl;
    for (int& _it : _result) {
        cout << _it << endl;
    }
    cout.flush();

    return 0;
}

int main() {

    AbstractWars abstractWars;

    // do edit codes if you need

    call_init(abstractWars);

    call_sendTroops(abstractWars);

    return 0;
}
```
C++はVS2015でしかコンパイル確認してない(実行テストはしてない)  
怖いね…   


#### Definitionの例から生成したC#コード

```c#
using System;
using System.Collections.Generic;

public class AbstractWars
{

    public int init(int[] baseLocations, int speed)
    {
        return 0;
    }

    public int[] sendTroops(int[] bases, int[] troops)
    {
        return new int[0];
    }

    // -------8<------- end of solution submitted to the website -------8<-------

    static int CallInit(AbstractWars abstractWars)
    {
        int _baseLocationsSize = int.Parse(Console.ReadLine());
        int[] baseLocations = new int[_baseLocationsSize];
        for (int _idx = 0; _idx < _baseLocationsSize; _idx++)
        {
            baseLocations[_idx] = int.Parse(Console.ReadLine());
        }
        int speed = int.Parse(Console.ReadLine());

        int _result = abstractWars.init(baseLocations, speed);

        Console.WriteLine(_result);
        Console.Out.Flush();

        return 0;
    }

    static int CallSendTroops(AbstractWars abstractWars)
    {
        int _basesSize = int.Parse(Console.ReadLine());
        int[] bases = new int[_basesSize];
        for (int _idx = 0; _idx < _basesSize; _idx++)
        {
            bases[_idx] = int.Parse(Console.ReadLine());
        }
        int _troopsSize = int.Parse(Console.ReadLine());
        int[] troops = new int[_troopsSize];
        for (int _idx = 0; _idx < _troopsSize; _idx++)
        {
            troops[_idx] = int.Parse(Console.ReadLine());
        }

        int[] _result = abstractWars.sendTroops(bases, troops);

        Console.WriteLine(_result.Length);
        foreach (int _it in _result)
        {
            Console.WriteLine(_it);
        }
        Console.Out.Flush();

        return 0;
    }

    public static void Main()
    {
        AbstractWars abstractWars = new AbstractWars();

        // do edit codes if you need

        CallInit(abstractWars);

        CallSendTroops(abstractWars);

    }

}
```


#### Definitionの例から生成したPythonコード

```python
class AbstractWars:
    
    def init(self, baseLocations, speed):
        return 0
    
    def sendTroops(self, bases, troops):
        return []

# -------8<------- end of solution submitted to the website -------8<-------

from sys import stdout

def call_init(abstractWars):
    
    baseLocationsSize_ = int(raw_input())
    baseLocations = [int(raw_input()) for i in xrange(baseLocationsSize_)]
    speed = int(raw_input())
    
    result_ = abstractWars.init(baseLocations, speed)
    
    print result_
    stdout.flush()
    
    return 0

def call_sendTroops(abstractWars):
    
    basesSize_ = int(raw_input())
    bases = [int(raw_input()) for i in xrange(basesSize_)]
    troopsSize_ = int(raw_input())
    troops = [int(raw_input()) for i in xrange(troopsSize_)]
    
    result_ = abstractWars.sendTroops(bases, troops)
    
    print len(result_)
    for it_ in result_:
        print it_
    stdout.flush()
    
    return 0

if __name__ == '__main__':
    
    abstractWars = AbstractWars()
    
    # do edit codes if you need
    
    call_init(abstractWars)
    
    call_sendTroops(abstractWars)

```

Pythonは実行環境ないためちゃんと動くか分からない   

どの言語でも変数名などに前置アンダースコアを用いるのは特別な意味を与えることが多く取り扱いを気をつけねばならない  
Pythonでも特別な意味があるようでそれは一応避けた  
実引数でリスト渡さないでtupleにして渡したほうがいいのだろうか…   
Pythonよく分からないがtupleでたぶん固定長配列ぽくなるのかなあ？  


